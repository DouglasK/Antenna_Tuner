#include <AccelStepper.h>
#include <MultiStepper.h>

/*
  Antenna Stepper Motor Control - noLEDs Yet.

  This program drives a bipolar stepper motor.
  The motor is attached to digital pins 8 - 11 of the Arduino.

  This will allow for speed controlled tuning,
  as well as reporting the current step # when the motor is stopped


  Created Oct 8, 2016
  Modified Oct 8, 2016
  by Douglas King

*/

const int stepsPerRevolution = 32;  // change this to fit the number of steps per revolution
// for your motor

// pin assignments
const int potPin = 0; // analog
const int fwdPin = 7;
const int fwdSlowPin = 6;
const int revSlowPin = 5;
const int revPin = 4;
const int ledPWRPin = 19;
const int ledRevPin = 18;
const int ledSlowPin = 17;
const int ledFwdPin = 16;


// hardware input variables
const int potSlop = 10;
float potVal = 0;
int fwdState = 0;
int fwdSlowState = 0;
int revState = 0;
int revSlowState = 0;


// motor position storage and limits
int motorPos = 0;
int motorMaxPos = 4096;
int motorMinPos = 0;
const float slowTrim = 0.1;

int fwdLast = 0;
int fwdSlowLast = 0;
int revLast = 0;
int revSlowLast = 0;
float potLast = 0;
float motorSpeed = 0.0 ;
float motorLastSpeed = 0.0;

/*
  can use EEProm, only store when motor stops moving for more than x seconds
  Add zero detection, w/ reset motor switch?
  Auto zero on power on?
*/

// initialize the stepper library on pins 8 through 11:
AccelStepper stepper(8, 8, 10, 9, 11, true); // Defaults to AccelStepper::FULL4WIRE (4 pins) on 2, 3, 4, 5

const float maxRPM = 1000;

void setup() {
  // set the LED that lights when motor is running fwd to OUTPUT
  pinMode(ledFwdPin, OUTPUT);
  pinMode(ledPWRPin, OUTPUT);
  pinMode(ledSlowPin, OUTPUT);
  pinMode(ledRevPin, OUTPUT);
  digitalWrite(ledPWRPin, HIGH);
  digitalWrite(ledFwdPin, LOW);
  digitalWrite(ledRevPin, LOW);
  digitalWrite(ledSlowPin, LOW);
  // set the speed at 60 rpm:
  stepper.setMaxSpeed(maxRPM);
  stepper.setSpeed(analogRead(potPin) * maxRPM / 1024);
  stepper.setCurrentPosition(motorPos);
  stepper.stop();
  // initialize the serial port:
  Serial.begin(9600);
}

void buttonResponse(
  const char *button_label,
  int &lastState,
  int currentState,
  float motorSpeed
) {
  if (currentState != lastState) {
    // Serial.print(button_label);
    if (currentState == HIGH) {
      // Serial.println(F(" went high " ));
      if ((motorSpeed > 0 && stepper.currentPosition() < motorMaxPos) ||
          (motorSpeed < 0 && stepper.currentPosition() > motorMinPos)) {
        stepper.setSpeed(motorSpeed);
      }
    } else {
      // Serial.println(F(" went low"));
      stepper.stop();
      stepper.setSpeed(0.0);
      motorPos = stepper.currentPosition();
    }
  }
  lastState = currentState;
}

void setLights(float &lastSpeed, float motorSpeed) {
  float curSpeed;

  curSpeed = stepper.speed();
  if (curSpeed != lastSpeed) {
    motorLastSpeed = curSpeed;
    if (curSpeed == 0.0) { // Stopped
      // turn off all motor speed LEDs
      digitalWrite(ledFwdPin, LOW);
      digitalWrite(ledRevPin, LOW);
      digitalWrite(ledSlowPin, LOW);
    } else if ( curSpeed > 0.0 ) { // Forward
      // turn on forward LED, turn off Rev LED
      digitalWrite(ledFwdPin, HIGH);
      digitalWrite(ledRevPin, LOW);
    } else { // Reverse
      // turn on reverse LED, turn off fwd LED
      digitalWrite(ledFwdPin, LOW);
      digitalWrite(ledRevPin, HIGH);
    }
    if (abs(curSpeed) == abs(motorSpeed) * slowTrim) {
      // turn on slow led.
      digitalWrite(ledSlowPin, HIGH);
    } else {
      // turn off slow led
      digitalWrite(ledSlowPin, LOW);
    }
  }
}

void loop() {
  // run the stepper if needed

  stepper.runSpeed();

  // read the inputs

  potVal = analogRead(potPin);

  //  motorState = stepper.isRunning();

  if (stepper.currentPosition() == motorMaxPos || stepper.currentPosition() == motorMinPos) {
    stepper.stop();
    stepper.setSpeed(0.0);
  }


  // handle the motor speed / LED coordination.
  setLights(motorLastSpeed, motorSpeed);

  // pot Changed
  if ((potLast + potSlop) < potVal || (potLast - potSlop) > potVal) {
    // calc the motor speed.
    motorSpeed = potVal * maxRPM / 1024 ;
    potLast = potVal;
  }

  buttonResponse("fwd", fwdLast, digitalRead(fwdPin), motorSpeed);
  buttonResponse("rev", revLast, digitalRead(revPin), motorSpeed * -1);
  buttonResponse("fwdSlow", fwdSlowLast, digitalRead(fwdSlowPin), motorSpeed * slowTrim);
  buttonResponse("revSlow", revSlowLast, digitalRead(revSlowPin), motorSpeed * -1 * slowTrim);

}
